import React, { Component } from 'react';
import Resultado from './Resultado';

class Resumen extends Component {
  mostrarResumen = () => {
    const {marca,anio,plan} = this.props.infoAuto;
    if(!marca || !anio || !plan) return null;
    return(
      <div className="resumen">
        <h2>Resumen</h2>
        <ul>
          <li>Marca: {marca.replace(/\b\w/g, l => l.toUpperCase())}</li>
          <li>Año: {anio}</li>
          <li>Plan: {plan.replace(/\b\w/g, l => l.toUpperCase())}</li>
        </ul>

      </div>
    )
  }
  render() {
    return(
      <div>
        {this.mostrarResumen()}
        <Resultado
            precio={this.props.precio}
          />
      </div>
    )
  }
}

export default Resumen;
