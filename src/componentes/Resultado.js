import React, { Component } from 'react';
import {TransitionGroup, CSSTransition} from 'react-transition-group';

class Resultado extends Component {
  render() {
    const precio = this.props.precio;
    const banner = (!precio) ? 'Elije marca, año y plan' : `$ ${precio}`
    return(
      <div className="gran-total">
        <TransitionGroup component="span" className="resultado">
          <CSSTransition
            classNames="resultado"
            key={precio}
            timeout={{enter: 500, leave: 500}}
            >
            <span>{banner}</span>
          </CSSTransition>
        </TransitionGroup>
      </div>
    )
  }
}

export default Resultado;
