import React, { Component } from 'react';
import Header from './Header';
import Form from './Form';
import Resumen from './Resumen';

class App extends Component {
  state = {
    precio: '',
    infoAuto: {}
  }
  cotizarSeguro = (infoAuto) => {
    console.log("Datos recibidos:");
    console.log(infoAuto);
    const {marca, anio, plan} = infoAuto;
    const aniobase = 2008;
    const difanio = anio - aniobase;
    let precio = 2000;
    let inmar = 1;
    let inplan = 1;
    switch(marca) {
      case "americano":
        inmar = 1.2;
        break;
      case "europeo":
        inmar = 1.6;
        break;
      case "asiatico":
        inmar = 1.15;
        break;
      default:
        inmar = 1;
        break;
    }
    switch(plan) {
      case "completo":
        inplan = 1.45;
        break;
      default:
        inplan = 0.999999;
        break;
    }
    let inanio = (difanio * 0.0611111)+1;
    precio = parseFloat(precio * inmar * inanio * inplan).toFixed(2);
    this.setState({
      precio: precio,
      infoAuto: infoAuto
    });
    console.log(`El precio es: ${precio}`);

  }
  render() {
    return (
      <div className="contenedor">
        <Header
            titulo="Cotizador de Seguro de Automóvil"
          />
        <div className="contenedor-formulario">
            <Form
                cotizarSeguro={this.cotizarSeguro}
              />
            <Resumen
                precio={this.state.precio}
                infoAuto={this.state.infoAuto}
              />
        </div>
      </div>
    );
  }
}

export default App;
